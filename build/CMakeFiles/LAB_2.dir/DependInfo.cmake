# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  )
# The set of files for implicit dependencies of each language:

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "G4_STORE_TRAJECTORY"
  "G4VERBOSE"
  "G4UI_USE"
  "G4VIS_USE"
  "G4UI_USE_TCSH"
  "G4INTY_USE_QT"
  "G4UI_USE_QT"
  "G4VIS_USE_OPENGLQT"
  "G4INTY_USE_XT"
  "G4VIS_USE_OPENGLX"
  "G4VIS_USE_OPENGL"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/yura/Software/geant4_9_6_p03/install/include/Geant4"
  "/usr/local/qt/4.8.5/include"
  "/usr/local/qt/4.8.5/include/QtCore"
  "/usr/local/qt/4.8.5/include/QtGui"
  "/usr/local/qt/4.8.5/include/QtOpenGL"
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
